#include "Adafruit_Arcada.h"
#include <Adafruit_NeoPixel.h>
#include "Graphics.h"

//Arcada
Adafruit_Arcada arcada;

//Neopixel
#define NUM_LEDS 57
#define PIN        8
#define NUMPIXELS 5
Adafruit_NeoPixel strip(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);

//Music
File musicFile;
bool musicIsPlaying = false;
wavStatus status;

//Game 
bool start_Game;
bool game_over;
int score = 0;
int counter  = 0;
int countertwo = 0;
int lifeNr = 4;

//button
uint8_t old_Button_State = 0;
uint8_t new_Button_State = 0;

// Boggy Movement
int spaceschiffy = 10;
int boggyx = 160;
long randNumber = 0;
int randomnumber1 = 106;

void setup() {

  Serial.begin(9600);
  Serial.print("Welcome to the Space Gameplay");

  // arcada init
  if (!arcada.arcadaBegin()) {
    Serial.print("Failed to begin");
    while (1) delay(10);
  }

  // Dispaly Init
  arcada.displayBegin();

  //sounds init
  arcada.filesysBegin();
  arcada.filesysBeginMSD();

  // Turn on backlight
  arcada.setBacklight(255); 
  
  //Neopixels init
  strip.begin();

  //Game init 
  arcada.display->fillScreen(ARCADA_WHITE);
  Game_Init();
}

void loop() {

  ButtonStatus_Watcher();
  CounterUpdate();
  music_main_Loop();

}

/* 
  Reads the Button State and call the method of each Button
*/
void ButtonStatus_Watcher(){
     new_Button_State = arcada.readButtons();
    if( old_Button_State !=new_Button_State ){
    switch(new_Button_State)
      {
        case ARCADA_BUTTONMASK_A:  GameStart() ; start_Game = true ; game_over = false; break;
        case ARCADA_BUTTONMASK_B: Game_Init(); start_Game= false; game_over= false ;break;
        case ARCADA_BUTTONMASK_UP: if(start_Game && 26 <= spaceschiffy ){arcada.display->fillRect(0, 10, 24, 118, ARCADA_BLACK);spaceschiffy -= 16; arcada.display->drawRGBBitmap(0, spaceschiffy, spaceschiff, 24, 16);}; break;
        case ARCADA_BUTTONMASK_DOWN : if(start_Game &&spaceschiffy <= 90){arcada.display->fillRect(0, 10, 24, 118, ARCADA_BLACK);spaceschiffy += 16; arcada.display->drawRGBBitmap(0, spaceschiffy, spaceschiff, 24, 16);}; break;
      }
      old_Button_State = new_Button_State;
    }
}

/* 
  Draw the main Screen icons
*/
void Game_Init(){
  turnOffPixels();
  arcada.display->setTextColor(ARCADA_BLACK);
  arcada.display->setTextWrap(false);
  arcada.display->fillScreen(ARCADA_WHITE);
  arcada.display->drawRGBBitmap(0, 0, logo,160, 128);
  arcada.display->setTextColor(ARCADA_RED);
  arcada.display->setCursor(10, 20);
  arcada.display->setTextSize(3);
  arcada.display->println("To Start");
  arcada.display->setCursor(10, 50);
  arcada.display->println("The Game");
  arcada.display->setTextColor(ARCADA_BLACK);
  arcada.display->setCursor(15, 80);
  arcada.display->println("Press A");
}

/* 
   Draw the GamePlay Screen icons
*/
void GameStart(){
    OLED_init();
    arcada.display->fillScreen(ARCADA_BLACK);
    arcada.display->setTextColor(ARCADA_RED);
    arcada.display->setTextSize(0);
    arcada.display->setCursor(120, 0);
    arcada.display->println("B(EXIT)");
    arcada.display->drawRGBBitmap(0,0, Heart_icon, 10, 10);
    arcada.display->drawRGBBitmap(10,0, Heart_icon, 10, 10);
    arcada.display->drawRGBBitmap(20,0, Heart_icon, 10, 10);
    arcada.display->drawRGBBitmap(30,0, Heart_icon, 10, 10);
    arcada.display->drawRGBBitmap(40,0, Heart_icon, 10, 10);
    arcada.display->setTextColor(ARCADA_WHITE);
    arcada.display->setCursor(52, 0);
    arcada.display->println("Score:");
    arcada.display->setCursor(93, 0);
    arcada.display->println(score);
    score = 0;
    lifeNr = 4;
    counter  = 0;
    arcada.display->drawRGBBitmap(0, spaceschiffy, spaceschiff, 24, 16);
    arcada.display->drawFastHLine(0,9,160,ARCADA_GREEN);
}

/* 
  Draws the Gameover Screen and after 1000 ms update the Screen to main Screen 
*/
void Gameover(){
    game_over = true;
    arcada.display->fillScreen(ARCADA_BLACK);
    arcada.display->drawRGBBitmap(0, 0, gameover,160, 128);
    arcada.display->setTextSize(2);
    arcada.display->setTextColor(ARCADA_BLACK);
    arcada.display->setCursor(55, 75);
    arcada.display->println("Score");
    arcada.display->setCursor(60, 90);
    arcada.display->println(score);
    musicPlayer(0);
}

/* 
  Draws the boggy move
*/
void Boggy_move(){
    if(boggyx != -16){
      arcada.display->drawRGBBitmap(boggyx,randomnumber1, boggy, 16, 16);
      boggyx--;
     delay(1);
      if(boggyx == 22){Lost_Life(randomnumber1);}
        }else{boggyx = 160; randomnumber1 = randomnumber(); arcada.display->fillRect(24, 10, 136, 118, ARCADA_BLACK);}
}

/* 
  Return a randomnumber for the movement of the Boggys
*/
int randomnumber(){ 
   randNumber = random(0, 7);
   int x = 0;
   switch(randNumber){
    case 0 : x = 10;  break;
    case 1 : x = 26;  break;
    case 2 : x = 42;  break;
    case 3 : x = 58;  break;
    case 4 : x = 74;  break;
    case 5 : x = 90;  break;
    case 6 : x = 106; break;
    default: x = 106;  break;
   }
   return x;
} 

/* 
   Reads the LifeLost case and Handle it using the Tone and the Neopixels 
*/
void Lost_Life(int boyggyplace){

  if( boyggyplace == spaceschiffy ){
   arcada.display->fillRect(lifeNr*10 ,0,10,9,ARCADA_BLACK);
  //  arcada.display->fillRect(22,spaceschiffy+3,2,10,ARCADA_YELLOW);
  arcada.display->drawCircle(24,spaceschiffy+8,3,ARCADA_PINK );
   switch(lifeNr){
   case 4: strip.fill(strip.Color(50, 0, 0), 4, 1);strip.fill(strip.Color(0, 50, 0), 0, 4); break;
   case 3: strip.fill(strip.Color(50, 0, 0), 3, 2);strip.fill(strip.Color(0, 50, 0), 0, 3); break;
   case 2: strip.fill(strip.Color(50, 0, 0), 2, 3);strip.fill(strip.Color(0, 50, 0), 0, 2); break;
   case 1: strip.fill(strip.Color(50, 0, 0), 1, 4);strip.fill(strip.Color(0, 50, 0), 0, 1); break;
   case 0: strip.fill(strip.Color(50, 0, 0), 0, 5);break;
   default:  strip.fill(strip.Color(50, 40, 0), 0, 5);break;
  }
   strip.setBrightness(70);
   strip.show();
   lifeNr--;
   if(lifeNr >= 0){
   musicPlayer(1);}
   delay(500);
   arcada.display->drawRGBBitmap(0, spaceschiffy, spaceschiff, 24, 16);
   if (lifeNr == -1){Gameover(); lifeNr = 4;}
   boggyx= -16;
  }
}

/* 
  Remove the old Score, update the Value of it and draw the new Score
*/
void Score_Update(){
      score +=1;
      arcada.display->fillRect(90, 0, 30, 8, ARCADA_BLACK);
      arcada.display->setCursor(90, 0);
      arcada.display->println(score);
}

/* 
  Set the counter to update the Score each 100 ms and a counter for the GAMEOVER Screen
*/
void CounterUpdate(){
  if(start_Game){
    counter++;
     Boggy_move();
    if (counter == 100){
        Score_Update();
        counter = 0;
    }
  }
  if(game_over){
    countertwo++;
    start_Game= false;
    score = 0;
    if(countertwo == 70000){
       Game_Init(); 
       game_over = false;
       countertwo = 0;
    }
  }
}
/* 
   init the Neopixels with the green Color
*/
void OLED_init(){
         turnOffPixels();
        for (byte i = 0; i < NUM_LEDS; i++) {
                 strip.setPixelColor(i, strip.Color(0, 50, 0));
            } 
        strip.setBrightness(70);
        strip.show();
  }

/* 
   Turns the Neopixels Light off
*/
void turnOffPixels(){
    for (byte i = 0; i < NUM_LEDS; i++) {
        strip.setPixelColor(i, strip.Color(0, 0, 0));
    }
    strip.show();
}
/* 
  Reads the music File and play it using the TimeInterrupt Callback to play the next Simple
*/
void musicPlayer(int x){
  musicFile = arcada.openFileByIndex("/music", x, O_READ, "wav");
  if (!musicFile) {
    Serial.println("Fatal error: No sound file present.");
  }
  Serial.println("Loading sound file...");
  uint32_t sampleRate;
   status = arcada.WavLoad(musicFile, &sampleRate);
  if ((status == WAV_LOAD) || (status == WAV_EOF)) {
    arcada.enableSpeaker(true);
    arcada.timerCallback(sampleRate, TimeCallback);
  }
   musicIsPlaying = true;
}
/* 
  Reads the music File and play it using the TimeInterrupt Callback to play the next Simple
*/
void TimeCallback(void){
  wavStatus status = arcada.WavPlayNextSample();
   if (status == WAV_EOF) {
    arcada.timerStop();
    arcada.enableSpeaker(false);
    musicIsPlaying = false;
    musicFile.close();
  }else {
    musicIsPlaying = true;
  }
}
/* 
 fill music buffer if needed
*/
void music_main_Loop(){
  if (musicIsPlaying) {
    if (arcada.WavReadyForData()) {
      status = arcada.WavReadFile();
    }
  } 
}